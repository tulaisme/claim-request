﻿using G2.ClaimRequest.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace G2.ClaimRequest.Services.IRepositories
{
    public interface IClaimRepository : IBaseRepository<Claim>
    {
        IEnumerable<Claim> GetClaimDraft(string staffId);
        IEnumerable<Claim> GetClaimPenddingApproval(string staffId);
        IEnumerable<Claim> GetClaimRejected(string staffId);
        IEnumerable<Claim> GetClaimApproved(string staffId);
        IEnumerable<Claim> GetClaimCancelled(string staffId);
        IEnumerable<Claim> GetClaimPaid(string staffId);
        
    }
}
