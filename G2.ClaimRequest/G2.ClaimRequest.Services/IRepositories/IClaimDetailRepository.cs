﻿using G2.ClaimRequest.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace G2.ClaimRequest.Services.IRepositories
{
    public interface IClaimDetailRepository : IBaseRepository<ClaimDetail>
    {
        IEnumerable<ClaimDetail> GetClaimDetailForClaimAndStaff(int claimId, string staffId);
    }
}
