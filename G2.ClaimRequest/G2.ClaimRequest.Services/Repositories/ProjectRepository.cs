﻿using G2.ClaimRequest.Core.Data;
using G2.ClaimRequest.Core.Models;
using G2.ClaimRequest.Services.IRepositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace G2.ClaimRequest.Services.Repositories
{
    public class ProjectByClaim
    {
        public string ProjectName { set; get; }
        public DateTime From { set; get; }
        public DateTime To { set; get; }
        public string PMName { get; set; }
    }
    public class ProjectRepository : BaseRepository<Project>, IProjectRepository
    {   
       

        private readonly ClaimDbContext context = new ClaimDbContext();
        public ProjectByClaim GetProjectNameByClaim(int claimId)
        {
            var query = from p in context.Projects
                        join j in context.PositionInProjects on p.ProjectId equals j.ProjectId
                        join s in context.Staffs on j.StaffId equals s.Id
                        join c in context.Claims on j.StaffId equals c.StaffId
                        where c.ClaimId == claimId
                        select new ProjectByClaim()
                        {
                            ProjectName = p.ProjectName,
                            From = p.From,
                            To = p.To,
                            PMName = p.PM.StaffName
                        };
            return query.FirstOrDefault();
        }
    }
}
