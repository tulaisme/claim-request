﻿using G2.ClaimRequest.Core.Data;
using G2.ClaimRequest.Core.Models;
using G2.ClaimRequest.Services.IRepositories;
using System.Collections.Generic;
using System.Linq;

namespace G2.ClaimRequest.Services.Repositories
{
    public class ClaimDetailRepository : BaseRepository<ClaimDetail>, IClaimDetailRepository
    {
        private readonly ClaimDbContext context;
        public IEnumerable<ClaimDetail> GetClaimDetailForClaimAndStaff(int claimId, string staffId)
        {
            return context.ClaimDetails.Where(x => x.ClaimId == claimId && x.Claim.StaffId == staffId).ToList();
        }
    }
}
