﻿using G2.ClaimRequest.Core.Data;
using G2.ClaimRequest.Core.Models;
using G2.ClaimRequest.Services.IRepositories;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;

namespace G2.ClaimRequest.Services.Repositories
{
    public class ClaimRepository : BaseRepository<Claim>, IClaimRepository
    {     
        public IEnumerable<Claim> GetClaimApproved(string staffId)
        {
            return _db.Claims.Where(x => x.Status == Status.Approved && x.StaffId == staffId).ToList();
        }

        public IEnumerable<Claim> GetClaimCancelled(string staffId)
        {
            return _db.Claims.Where(x => x.Status == Status.Cancelled && x.StaffId == staffId).ToList();
        }

        public IEnumerable<Claim> GetClaimDraft(string staffId)
        {
            return _db.Claims.Where(x => x.Status == Status.Draft && x.StaffId == staffId).ToList();
        }

        public IEnumerable<Claim> GetClaimPaid(string staffId)
        {
            return _db.Claims.Where(x => x.Status == Status.Paid && x.StaffId == staffId).ToList();
        }

        public IEnumerable<Claim> GetClaimPenddingApproval(string staffId)
        {
            return _db.Claims.Where(x => x.Status == Status.PenddingApproval && x.StaffId == staffId).ToList();
        }

        public IEnumerable<Claim> GetClaimRejected(string staffId)
        {
            return _db.Claims.Where(x => x.Status == Status.Rejected && x.StaffId == staffId).ToList();
        }

        public override void Update(Claim entity)
        {
            ////var oldEntity = _db.Claims.Include(x => x.ClaimDetails).SingleOrDefault(e => e.ClaimId == entity.ClaimId);
            ////oldEntity.

           
            var claim = GetById(entity.ClaimId);
            //var claimDetailxx = claim.ClaimDetails.Take(5);
            //claim.ClaimDetails = entity.ClaimDetails;
            //var last = claim.ClaimDetails.Last();

            //Xoa 1 cai claimdetails trong Claim, 
            //Claim detail bat buoc phai thuoc ve 1 claim
            //claim.ClaimDetails = new List<ClaimDetail>();
            claim.ClaimDetails = entity.ClaimDetails;

            _db.Entry(claim).State = EntityState.Modified;

            _db.SaveChanges(); 
        }
    }
}
