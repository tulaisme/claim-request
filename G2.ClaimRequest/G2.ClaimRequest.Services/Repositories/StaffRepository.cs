﻿using G2.ClaimRequest.Core.Data;
using G2.ClaimRequest.Core.Models;
using G2.ClaimRequest.Services.IRepositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace G2.ClaimRequest.Services.Repositories
{
    public class StaffRepository : BaseRepository<Staff>, IStaffRepository
    {
        public readonly ClaimDbContext context = new ClaimDbContext();

        public Staff GetStaffById(string id)
        {
            return context.Staffs.Where(x => x.Id == id).FirstOrDefault();
        }
    }
}
