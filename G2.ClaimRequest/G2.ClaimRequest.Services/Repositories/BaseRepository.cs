﻿using G2.ClaimRequest.Core.Data;
using G2.ClaimRequest.Services.IRepositories;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace G2.ClaimRequest.Services.Repositories
{
    public class BaseRepository<T> : IBaseRepository<T> where T : class
    {
        protected readonly ClaimDbContext _db;
        protected DbSet<T> dbSet;

        public BaseRepository()
        {
            _db = new ClaimDbContext();
            dbSet = _db.Set<T>();
        }

        public void Add(T entity)
        {
            dbSet.Add(entity);
        }

        public void Delete(T entity)
        {
            //dbSet.Remove(entity);
            dbSet.Attach(entity);
            _db.Entry(entity).State = EntityState.Deleted;
        }

        public void DeleteById(int id)
        {
            Delete(dbSet.Find(id));          
        }

        public T GetById(int id)
        {
            var entity = dbSet.Find(id);

            //_db.Entry(entity).State = EntityState.Detached;

            return entity;
        }

        public IEnumerable<T> GetAll(
       Expression<Func<T, bool>> filter = null,
       Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
       string includeProperties = null
       )
        {
            IQueryable<T> query = dbSet;

            // Filter or not.
            if (filter != null)
            {
                query = query.Where(filter);
            }

            // Call eager loading or not.
            if (includeProperties != null)
            {
                foreach (var item in includeProperties.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    query = query.Include(item);
                }
            }

            // Orderby query or not
            if (orderBy != null)
            {
                return orderBy(query).ToList();
            }

            return query.ToList();
        }

        public virtual void Update(T entity)
        {
            _db.Entry(entity).State = EntityState.Modified;
            _db.SaveChanges();
        }

        public void Commit()
        {
            _db.SaveChanges();
        }
    }
}
