﻿using G2.ClaimRequest.Core.Data;
using G2.ClaimRequest.Core.Models;
using G2.ClaimRequest.Services.IRepositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace G2.ClaimRequest.Services.Repositories
{
    public class PositionInProjectRepository : BaseRepository<PositionInProject>, IPositionInProjectRepository
    {
        private readonly ClaimDbContext context = new ClaimDbContext();
        public void DeleteByProject(int projectId)
        {
            var listPosition = context.PositionInProjects.Where(x => x.ProjectId == projectId).ToList();
            foreach (var position in listPosition)
            {
                context.PositionInProjects.Remove(position);
            }
            context.SaveChanges();
        }
    }
}
