﻿using G2.ClaimRequest.Services.Repositories;
using G2.ClaimRequest.Web.Mapping;
using G2.ClaimRequest.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace G2.ClaimRequest.Web.Business
{
    public class PositionInProjectBusiness
    {
        public PositionInProjectRepository positionInProjectRepository = new PositionInProjectRepository();
        public List<PositionInProjectViewModel> GetAll()
        {
            // vì muốn chuyển sang automapper rồi nên chỗ này tự túc nhé
            var listPosition = positionInProjectRepository.GetAll().Select(x => x.ToModel()).ToList();
            return listPosition;
        }

        public void Add(PositionInProjectViewModel position)
        {
            positionInProjectRepository.Add(position.ToEntity());
            positionInProjectRepository.Commit();
        }

        public void Delete(int projectId)
        {
            positionInProjectRepository.DeleteByProject(projectId);
            positionInProjectRepository.Commit();
        }
    }
}