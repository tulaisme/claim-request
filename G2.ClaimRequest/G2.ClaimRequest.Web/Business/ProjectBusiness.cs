﻿using G2.ClaimRequest.Services.IRepositories;
using G2.ClaimRequest.Services.Repositories;
using G2.ClaimRequest.Web.Mapping;
using G2.ClaimRequest.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace G2.ClaimRequest.Web.Business
{
    public class ProjectBusiness
    {
        public ProjectRepository projectRepository = new ProjectRepository(); 
        public List<ProjectViewModel> GetAll()
        {
            // vì muốn chuyển sang automapper rồi nên chỗ này tự túc nhé
            var listProject = projectRepository.GetAll().Select(x => x.ToModel()).ToList();
            return listProject;
        }

        public void Add(ProjectViewModel project)
        {
            projectRepository.Add(project.ToEntity());
            projectRepository.Commit();
        }

        public ProjectViewModel Detail(int? id)
        {
            var project = projectRepository.GetById(id.Value).ToModel();
            return project;
        }

        public void Delete(int id)
        {
            var project = projectRepository.GetById(id);
            projectRepository.Delete(project);
            projectRepository.Commit();
        }

        public void Update(ProjectViewModel project)
        {
            projectRepository.Update(project.ToEntity());
            projectRepository.Commit();
        }

        public string GetProjectName(int claimId)
        {
            return projectRepository.GetProjectNameByClaim(claimId).ProjectName;
        }

        public string GetPMName(int claimId)
        {
            return projectRepository.GetProjectNameByClaim(claimId).PMName;
        }
    }
}