﻿using G2.ClaimRequest.Services.IRepositories;
using G2.ClaimRequest.Services.Repositories;
using G2.ClaimRequest.Web.Mapping;
using G2.ClaimRequest.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace G2.ClaimRequest.Web.Business
{
    public class StaffBusiness
    {
        public StaffRepository staffRepository = new StaffRepository();

        public List<StaffViewModel> GetAll()
        {
            // vì muốn chuyển sang automapper rồi nên chỗ này tự túc nhé
            var listStaff = staffRepository.GetAll().Select(x => x.ToModel()).ToList();
            return listStaff;
        }

        public StaffViewModel Detail(string id)
        {
            var staff = staffRepository.GetStaffById(id).ToModel();
            return staff;
        }

        public void Add(StaffViewModel staff)
        {
            staffRepository.Add(staff.ToEntity());
            staffRepository.Commit();
        }

        public void Delete(string id)
        {
            var staff = staffRepository.GetStaffById(id);
            staffRepository.Delete(staff);
            staffRepository.Commit();
        }

        public void Update(StaffViewModel staff)
        {
            staffRepository.Update(staff.ToEntity());
            staffRepository.Commit();
        }

        public string GetStaffName(string staffId)
        {
            var staff = staffRepository.GetStaffById(staffId).ToModel();
            return staff.StaffName;
        }
    }
}