﻿using G2.ClaimRequest.Services.IRepositories;
using G2.ClaimRequest.Services.Repositories;
using G2.ClaimRequest.Web.Mapping;
using G2.ClaimRequest.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace G2.ClaimRequest.Web.Business
{
    public class ClaimBusiness
    {
        public ClaimRepository claimRepository = new ClaimRepository();
        public List<ClaimViewModel> GetAll()
        {
            var listClaim = claimRepository.GetAll().Select(x => x.ToModel()).ToList();
            return listClaim;
        }
        public void Add(ClaimViewModel claim)
        {
            claimRepository.Add(claim.ToEntity());
            claimRepository.Commit();
        }
        public ClaimViewModel Detail(int? id)
        {
            var claim = claimRepository.GetById(id.Value).ToModel();
            return claim;
        }

        public void Delete(int id)
        {
            var claim = claimRepository.GetById(id);
            claimRepository.Delete(claim);
            claimRepository.Commit();
        }
        public void Update(ClaimViewModel claim)
        {
            
            claimRepository.Update(claim.ToEntity());
            claimRepository.Commit();
        }

        public ClaimViewModel GetLastestClaim()
        {
            var claim = claimRepository.GetAll().OrderByDescending(c => c.ClaimId).FirstOrDefault();
            return claim.ToModel();
        }
    }
}