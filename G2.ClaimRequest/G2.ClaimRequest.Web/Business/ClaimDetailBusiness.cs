﻿using G2.ClaimRequest.Services.IRepositories;
using G2.ClaimRequest.Services.Repositories;
using G2.ClaimRequest.Web.Mapping;
using G2.ClaimRequest.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace G2.ClaimRequest.Web.Business
{
    public class ClaimDetailBusiness
    {

        public ClaimDetailRepository claimRepository = new ClaimDetailRepository();
        public List<ClaimDetailViewModel> GetAll()
        {
            // vì muốn chuyển sang automapper rồi nên chỗ này tự túc nhé
            var listClaimDetail = claimRepository.GetAll().Select(x => x.ToModel()).ToList();
            return listClaimDetail;
        }
        public void Add(ClaimDetailViewModel claimDetail)
        {
            claimRepository.Add(claimDetail.ToEntity());
            claimRepository.Commit();
        }


        public ClaimDetailViewModel Detail(int? id)
        {
            var claimDetail = claimRepository.GetById(id.Value).ToModel();
            return claimDetail;
        }
        public void Delete(int id)
        {
            var claimDetail = claimRepository.GetById(id);
            claimRepository.Delete(claimDetail);
            claimRepository.Commit();
        }
        public void Update(ClaimDetailViewModel claimDetail)
        {
            claimRepository.Update(claimDetail.ToEntity());
            claimRepository.Commit();
        }
    }
}