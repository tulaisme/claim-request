﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace G2.ClaimRequest.Web.Models
{
    public class PositionInProjectViewModel
    {
        public PositionInProjectViewModel(string staffId, int projectId, string position)
        {
            StaffId = staffId;
            ProjectId = projectId;
            Position = position;
        }

        public string StaffId { get; set; }
        public virtual StaffViewModel Staff { get; set; }

        public int ProjectId { get; set; }
        public virtual ProjectViewModel Project { get; set; }

        public string Position { get; set; }
    }
}