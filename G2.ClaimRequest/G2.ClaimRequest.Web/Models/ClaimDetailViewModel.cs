﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace G2.ClaimRequest.Web.Models
{
    public class ClaimDetailViewModel
    {
        public int ClaimDetailId { get; set; }
        public int ClaimId { get; set; }
        public DateTime Date { get; set; }
        public DateTime FromTimeWork { get; set; }
        public DateTime ToTimeWork { get; set; }
        public decimal TotalWorkHours { get; set; }
        public bool IsAdd { get; set; }
        public string Remarks { get; set; }
    }
}