﻿using G2.ClaimRequest.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace G2.ClaimRequest.Web.Models
{
    public class ClaimViewModel
    {
        public ClaimViewModel()
        {
            RelativeClaim = new List<ClaimViewModel>();
            ClaimDetails = new List<ClaimDetailViewModel>();
        }
        public int ClaimId { get; set; }
        public Status Status { get; set; }
        public decimal TotalWorkingHour { get; set; }
        public string Remarks { get; set; }
        public string AuditTrail { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string StaffId { get; set; }
        public virtual Staff Staff { get; set; }
        public virtual IList<ClaimDetailViewModel> ClaimDetails { get; set; }

        public List<ClaimViewModel> RelativeClaim { set; get; }
        public List<int> SelectedClaims { get; set; }
    }
    public enum Status
    {
        Draft,
        [Display(Name = "Pendding Approval")]
        PenddingApproval,
        Approved,
        Paid,
        Rejected,
        Cancelled
    }
}