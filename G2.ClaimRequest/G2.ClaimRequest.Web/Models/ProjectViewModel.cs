﻿using G2.ClaimRequest.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace G2.ClaimRequest.Web.Models
{
    public class ProjectViewModel
    {
        public int ProjectId { get; set; }
        public string ProjectName { get; set; }
        public string ProjectCode { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }
        [Display(Name = "PM")]
        public string PMId { get; set; }

        public virtual StaffViewModel PM { get; set; }
        public virtual StaffViewModel QA { get; set; }

        [Display(Name = "QA")]
        public string QAId { get; set; }
        public virtual ICollection<PositionInProject> Staffs { get; set; }

        [Display(Name="Technical Leads")]
        public IEnumerable<string> SelectedTechnicalLeads { get; set; }
        public IEnumerable<SelectListItem> TechnicalLeads { get; set; }

        [Display(Name = "BAs")]
        public IEnumerable<string> SelectedBAs { get; set; }
        public IEnumerable<SelectListItem> BAs { get; set; }

        [Display(Name = "Developers")]
        public IEnumerable<string> SelectedDevelopers { get; set; }
        public IEnumerable<SelectListItem> Developers { get; set; }

        [Display(Name = "Testers")]
        public IEnumerable<string> SelectedTesters { get; set; }
        public IEnumerable<SelectListItem> Testers { get; set; }

        [Display(Name = "Technical Consultancies")]
        public IEnumerable<string> SelectedTechnicalConsultancies { get; set; }
        public IEnumerable<SelectListItem> TechnicalConsultancies { get; set; }

    }
}