﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace G2.ClaimRequest.Web.Models
{
    public class StaffViewModel
    {
        public string Id { set; get; }
        public string Email { get; set; }
        public string StaffName { get; set; }
        public string Department { get; set; }
        public string JobRank { get; set; }
        public decimal Salary { get; set; }

        public virtual ICollection<ClaimViewModel> ClaimRequests { get; set; }
        public virtual ICollection<PositionInProjectViewModel> Projects { get; set; }
    }
}