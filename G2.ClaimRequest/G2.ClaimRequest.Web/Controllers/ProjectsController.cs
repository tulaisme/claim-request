﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using G2.ClaimRequest.Core.Data;
using G2.ClaimRequest.Core.Models;
using G2.ClaimRequest.Web.Business;
using G2.ClaimRequest.Web.Models;

namespace G2.ClaimRequest.Web.Controllers
{
    public class ProjectsController : Controller
    {
        public ProjectBusiness projectBusiness = new ProjectBusiness();

        public StaffBusiness staffBusiness = new StaffBusiness();

        public PositionInProjectBusiness positionBusiness = new PositionInProjectBusiness();

        // GET: Projects
        public ActionResult Index()
        {
            return View(projectBusiness.GetAll());
        }

        // GET: Projects/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProjectViewModel project = projectBusiness.Detail(id.Value);
            if (project == null)
            {
                return HttpNotFound();
            }
            return View(project);
        }

        // GET: Projects/Create
        public ActionResult Create()
        {
            ViewBag.PMId = new SelectList(staffBusiness.GetAll(), "Id", "Email");
            ViewBag.QAId = new SelectList(staffBusiness.GetAll(), "Id", "Email");
            var project = new ProjectViewModel();
            IQueryable<StaffViewModel> staff = staffBusiness.GetAll().AsQueryable<StaffViewModel>();
            project.TechnicalLeads = staff.Where(j => j.JobRank.StartsWith("TechnicalLead")).Select(i => new SelectListItem
            {
                Text = i.Email,
                Value = i.Id.ToString()
            });

            project.BAs = staffBusiness.GetAll().Where(i => i.JobRank.StartsWith("BA")).Select(i => new SelectListItem
            {
                Text = i.Email,
                Value = i.Id.ToString()
            });

            project.Developers = staffBusiness.GetAll().Where(i => i.JobRank.StartsWith("Developer")).Select(i => new SelectListItem
            {
                Text = i.Email,
                Value = i.Id.ToString()
            });

            project.TechnicalConsultancies = staffBusiness.GetAll().Where(i => i.JobRank.StartsWith("TechnicalConsultancy")).Select(i => new SelectListItem
            {
                Text = i.Email,
                Value = i.Id.ToString()
            });

            project.Testers = staffBusiness.GetAll().Where(i => i.JobRank.StartsWith("Tester")).Select(i => new SelectListItem
            {
                Text = i.Email,
                Value = i.Id.ToString()
            });

            return View(project);
        }

        // POST: Projects/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ProjectId,ProjectName,ProjectCode,From,To,Duration,PMId,QAId,SelectedTechnicalLeads,SelectedBAs,SelectedDevelopers,SelectedTesters,SelectedTechnicalConsultancies")] ProjectViewModel project)
        {

            if (ModelState.IsValid)
            {
                projectBusiness.Add(project);

                var projectId = projectBusiness.GetAll()
                .Where(x => x.ProjectCode == project.ProjectCode)
                .FirstOrDefault().ProjectId;

                if (project.SelectedTechnicalLeads != null)
                {
                    foreach (var item in project.SelectedTechnicalLeads)
                    {
                        positionBusiness.Add(new PositionInProjectViewModel(item, projectId, "TechnicalLead"));
                    }
                }

                if (project.SelectedBAs != null)
                {
                    foreach (var item in project.SelectedBAs)
                    {
                        positionBusiness.Add(new PositionInProjectViewModel(item, projectId, "BA"));
                    }
                }

                if (project.SelectedDevelopers != null)
                {
                    foreach (var item in project.SelectedDevelopers)
                    {
                        positionBusiness.Add(new PositionInProjectViewModel(item, projectId, "Developer"));
                    }
                }

                if (project.SelectedTechnicalConsultancies != null)
                {
                    foreach (var item in project.SelectedTechnicalConsultancies)
                    {
                        positionBusiness.Add(new PositionInProjectViewModel(item, projectId, "TechnicalConsultancy"));
                    }
                }

                if (project.SelectedTesters != null)
                {
                    foreach (var item in project.SelectedTesters)
                    {
                        positionBusiness.Add(new PositionInProjectViewModel(item, projectId, "Tester"));
                    }
                }

                return RedirectToAction("Index");
            }

            ViewBag.PMId = new SelectList(staffBusiness.GetAll(), "Id", "Email", project.PMId);
            ViewBag.QAId = new SelectList(staffBusiness.GetAll(), "Id", "Email", project.QAId);

            project.TechnicalLeads = staffBusiness.GetAll().Select(i => new SelectListItem
            {
                Text = i.Email,
                Value = i.Id.ToString()
            });
            project.BAs = staffBusiness.GetAll().Select(i => new SelectListItem
            {
                Text = i.Email,
                Value = i.Id.ToString()
            });

            project.Developers = staffBusiness.GetAll().Select(i => new SelectListItem
            {
                Text = i.Email,
                Value = i.Id.ToString()
            });

            project.TechnicalConsultancies = staffBusiness.GetAll().Select(i => new SelectListItem
            {
                Text = i.Email,
                Value = i.Id.ToString()
            });

            project.Testers = staffBusiness.GetAll().Select(i => new SelectListItem
            {
                Text = i.Email,
                Value = i.Id.ToString()
            });
            return View(project);
        }

        // GET: Projects/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProjectViewModel project = projectBusiness.Detail(id.Value);
            if (project == null)
            {
                return HttpNotFound();
            }

            ViewBag.PMId = new SelectList(staffBusiness.GetAll(), "Id", "Email", project.PMId);
            ViewBag.QAId = new SelectList(staffBusiness.GetAll(), "Id", "Email", project.QAId);

            

            //var project = new ProjectViewModel();

            IQueryable<StaffViewModel> staff = staffBusiness.GetAll().AsQueryable<StaffViewModel>();

            project.TechnicalLeads = staff.Where(j => j.JobRank.StartsWith("TechnicalLead")).Select(i => new SelectListItem
            {
                Text = i.Email,
                Value = i.Id.ToString()
            });

            project.BAs = staffBusiness.GetAll().Where(i => i.JobRank.StartsWith("BA")).Select(i => new SelectListItem
            {
                Text = i.Email,
                Value = i.Id.ToString()
            });

            project.Developers = staffBusiness.GetAll().Where(i => i.JobRank.StartsWith("Developer")).Select(i => new SelectListItem
            {
                Text = i.Email,
                Value = i.Id.ToString()
            });

            project.TechnicalConsultancies = staffBusiness.GetAll().Where(i => i.JobRank.StartsWith("TechnicalConsultancy")).Select(i => new SelectListItem
            {
                Text = i.Email,
                Value = i.Id.ToString()
            });

            project.Testers = staffBusiness.GetAll().Where(i => i.JobRank.StartsWith("Tester")).Select(i => new SelectListItem
            {
                Text = i.Email,
                Value = i.Id.ToString()
            });

            project.SelectedBAs = positionBusiness.GetAll().Where(x => x.ProjectId == project.ProjectId && x.Position.StartsWith("BA")).Select(s => s.StaffId);
            project.SelectedDevelopers = positionBusiness.GetAll().Where(x => x.ProjectId == project.ProjectId && x.Position.StartsWith("Developer")).Select(s => s.StaffId);
            project.SelectedTechnicalConsultancies = positionBusiness.GetAll().Where(x => x.ProjectId == project.ProjectId && x.Position.StartsWith("TechnicalConsultancy")).Select(s => s.StaffId);
            project.SelectedTechnicalLeads = positionBusiness.GetAll().Where(x => x.ProjectId == project.ProjectId && x.Position.StartsWith("TechnicalLead")).Select(s => s.StaffId);
            project.SelectedTesters = positionBusiness.GetAll().Where(x => x.ProjectId == project.ProjectId && x.Position.StartsWith("Tester")).Select(s => s.StaffId);

            return View(project);
        }

        // POST: Projects/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ProjectId,ProjectName,ProjectCode,From,To,Duration,PMId,QAId,SelectedTechnicalLeads,SelectedBAs,SelectedDevelopers,SelectedTesters,SelectedTechnicalConsultancies")] ProjectViewModel project)
        {
            if (ModelState.IsValid)
            {
                positionBusiness.Delete(project.ProjectId);

                if (project.SelectedTechnicalLeads != null)
                {
                    foreach (var item in project.SelectedTechnicalLeads)
                    {
                        positionBusiness.Add(new PositionInProjectViewModel(item, project.ProjectId, "TechnicalLead"));
                    }
                }

                if (project.SelectedBAs != null)
                {
                    foreach (var item in project.SelectedBAs)
                    {
                        positionBusiness.Add(new PositionInProjectViewModel(item, project.ProjectId, "BA"));
                    }
                }

                if (project.SelectedDevelopers != null)
                {
                    foreach (var item in project.SelectedDevelopers)
                    {
                        positionBusiness.Add(new PositionInProjectViewModel(item, project.ProjectId, "Developer"));
                    }
                }

                if (project.SelectedTechnicalConsultancies != null)
                {
                    foreach (var item in project.SelectedTechnicalConsultancies)
                    {
                        positionBusiness.Add(new PositionInProjectViewModel(item, project.ProjectId, "TechnicalConsultancy"));
                    }
                }

                if (project.SelectedTesters != null)
                {
                    foreach (var item in project.SelectedTesters)
                    {
                        positionBusiness.Add(new PositionInProjectViewModel(item, project.ProjectId, "Tester"));
                    }
                }





                projectBusiness.Update(project);
                return RedirectToAction("Index");
            }
            ViewBag.PMId = new SelectList(staffBusiness.GetAll(), "Id", "Email", project.PMId);
            ViewBag.QAId = new SelectList(staffBusiness.GetAll(), "Id", "Email", project.QAId);

            IQueryable<StaffViewModel> staff = staffBusiness.GetAll().AsQueryable<StaffViewModel>();

            project.TechnicalLeads = staff.Where(j => j.JobRank.StartsWith("TechnicalLead")).Select(i => new SelectListItem
            {
                Text = i.Email,
                Value = i.Id.ToString()
            });

            project.BAs = staffBusiness.GetAll().Where(i => i.JobRank.StartsWith("BA")).Select(i => new SelectListItem
            {
                Text = i.Email,
                Value = i.Id.ToString()
            });

            project.Developers = staffBusiness.GetAll().Where(i => i.JobRank.StartsWith("Developer")).Select(i => new SelectListItem
            {
                Text = i.Email,
                Value = i.Id.ToString()
            });

            project.TechnicalConsultancies = staffBusiness.GetAll().Where(i => i.JobRank.StartsWith("TechnicalConsultancy")).Select(i => new SelectListItem
            {
                Text = i.Email,
                Value = i.Id.ToString()
            });

            project.Testers = staffBusiness.GetAll().Where(i => i.JobRank.StartsWith("Tester")).Select(i => new SelectListItem
            {
                Text = i.Email,
                Value = i.Id.ToString()
            });

            project.SelectedBAs = positionBusiness.GetAll().Where(x => x.ProjectId == project.ProjectId && x.Position.StartsWith("BA")).Select(s => s.StaffId);
            project.SelectedDevelopers = positionBusiness.GetAll().Where(x => x.ProjectId == project.ProjectId && x.Position.StartsWith("Developer")).Select(s => s.StaffId);
            project.SelectedTechnicalConsultancies = positionBusiness.GetAll().Where(x => x.ProjectId == project.ProjectId && x.Position.StartsWith("TechnicalConsultancy")).Select(s => s.StaffId);
            project.SelectedTechnicalLeads = positionBusiness.GetAll().Where(x => x.ProjectId == project.ProjectId && x.Position.StartsWith("TechnicalLead")).Select(s => s.StaffId);
            project.SelectedTesters = positionBusiness.GetAll().Where(x => x.ProjectId == project.ProjectId && x.Position.StartsWith("Tester")).Select(s => s.StaffId);

            return View(project);
        }

        // GET: Projects/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProjectViewModel project = projectBusiness.Detail(id.Value);
            if (project == null)
            {
                return HttpNotFound();
            }
            return View(project);
        }

        // POST: Projects/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            projectBusiness.Delete(id);
            return RedirectToAction("Index");
        }

    }
}
