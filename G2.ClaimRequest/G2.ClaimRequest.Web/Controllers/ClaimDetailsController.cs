﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using G2.ClaimRequest.Core.Data;
using G2.ClaimRequest.Core.Models;
using G2.ClaimRequest.Web.Business;
using G2.ClaimRequest.Web.Models;

namespace G2.ClaimRequest.Web.Controllers
{
    public class ClaimDetailsController : Controller
    {
        public ClaimDetailBusiness claimDetailBusiness = new ClaimDetailBusiness();
        public List<ClaimDetailViewModel> listClaimDetail = new List<ClaimDetailViewModel>();
        // GET: ClaimDetails
        public ActionResult Index()
        {
            return View(claimDetailBusiness.GetAll());
        }

        // GET: ClaimDetails/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ClaimDetailViewModel claimDetail = claimDetailBusiness.Detail(id.Value);
            if (claimDetail == null)
            {
                return HttpNotFound();
            }
            return View(claimDetail);
        }

        // GET: ClaimDetails/Create
        public ActionResult Create()
        {
            ViewBag.ClaimId = new SelectList(claimDetailBusiness.GetAll(), "ClaimId", "Remarks");
            return View();
        }

        // POST: ClaimDetails/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ClaimDetailId,StartDate,EndDate,FromTimeWork,ToTimeWork,ClaimId")] ClaimDetailViewModel claimDetail)
        {
            if (ModelState.IsValid)
            {
                claimDetailBusiness.Add(claimDetail);
                return RedirectToAction("Index");
            }

            ViewBag.ClaimId = new SelectList(claimDetailBusiness.GetAll(), "ClaimId", "Remarks", claimDetail.ClaimId);
            return View(claimDetail);
        }

        // GET: ClaimDetails/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ClaimDetailViewModel claimDetail = claimDetailBusiness.Detail(id.Value);
            if (claimDetail == null)
            {
                return HttpNotFound();
            }
            ViewBag.ClaimId = new SelectList(claimDetailBusiness.GetAll(), "ClaimId", "Remarks", claimDetail.ClaimId);
            return View(claimDetail);
        }

        // POST: ClaimDetails/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ClaimDetailId,StartDate,EndDate,FromTimeWork,ToTimeWork,ClaimId")] ClaimDetailViewModel claimDetail)
        {
            if (ModelState.IsValid)
            {
                claimDetailBusiness.Update(claimDetail);
                return RedirectToAction("Index");
            }
            ViewBag.ClaimId = new SelectList(claimDetailBusiness.GetAll(), "ClaimId", "Remarks", claimDetail.ClaimId);
            return View(claimDetail);
        }

        // GET: ClaimDetails/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ClaimDetailViewModel claimDetail = claimDetailBusiness.Detail(id.Value);
            if (claimDetail == null)
            {
                return HttpNotFound();
            }
            return View(claimDetail);
        }

        // POST: ClaimDetails/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            claimDetailBusiness.Delete(id);
            return RedirectToAction("Index");
        }

    }
}
