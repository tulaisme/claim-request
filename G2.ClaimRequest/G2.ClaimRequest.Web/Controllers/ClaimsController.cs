﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EmailReminder;
using G2.ClaimRequest.Core.Data;
using G2.ClaimRequest.Core.Models;
using G2.ClaimRequest.Web.Business;
using G2.ClaimRequest.Web.Models;
using Microsoft.AspNet.Identity;
using OfficeOpenXml;
using OfficeOpenXml.Table;
using Status = G2.ClaimRequest.Web.Models.Status;

namespace G2.ClaimRequest.Web.Controllers
{
    [Authorize]
    public class ClaimsController : Controller
    {
        public ClaimBusiness claimBusiness = new ClaimBusiness();
        public ClaimDetailBusiness claimDetailBusiness = new ClaimDetailBusiness();

        public StaffBusiness staffBusiness = new StaffBusiness();
        public ProjectBusiness projectBusiness = new ProjectBusiness();
        public PositionInProjectBusiness positionInProjectBusiness = new PositionInProjectBusiness();

        public List<ClaimDetailViewModel> listClaimDetail = new List<ClaimDetailViewModel>();
        // GET: Claims
        [Authorize(Roles = "Claimer,Admin")]
        public ActionResult Index(string status)
        {
            IQueryable<ClaimViewModel> claims = claimBusiness.GetAll().AsQueryable<ClaimViewModel>();
            if (User.IsInRole("Claimer"))
            {
                claims = claims.Where(c => c.StaffId == User.Identity.GetUserId());
            };
            switch (status)
            {
                case "draft":
                    claims = claims.Where(c => c.Status == Status.Draft);
                    break;
                case "penddingApproval":
                    claims = claims.Where(c => c.Status == Status.PenddingApproval);
                    break;
                case "approved":
                    claims = claims.Where(c => c.Status == Status.Approved);
                    break;
                case "paid":
                    claims = claims.Where(c => c.Status == Status.Paid);
                    break;
                case "rejected":
                    claims = claims.Where(c => c.Status == Status.Rejected || c.Status == Status.Cancelled);
                    break;
                default:
                    break;
            }
            return View(claims.ToList());
        }

        [Authorize(Roles = "Approver")]
        public ActionResult ForMyVetting()
        {
            ViewBag.Title = "For my vetting";
            var projects = projectBusiness.GetAll().Where(x => x.PMId == User.Identity.GetUserId()).Select(p => p.ProjectId);
            var listStaff = new List<string>();
            foreach (var project in projects)
            {
                listStaff.AddRange(positionInProjectBusiness.GetAll().Where(p => p.ProjectId == project).Select(a => a.StaffId));
            }
            var claims = new List<ClaimViewModel>();
            foreach (var item in listStaff)
            {
                claims.AddRange(claimBusiness.GetAll().Where(c => c.StaffId == item && c.Status == Status.PenddingApproval));
            }
            return View(claims);
        }

        [Authorize(Roles ="Approver")]
        public ActionResult ApprovedOrPaid()
        {
            ViewBag.Title = "Approved or paid";
            var projects = projectBusiness.GetAll().Where(x => x.PMId == User.Identity.GetUserId()).Select(p => p.ProjectId);
            var listStaff = new List<string>();
            foreach (var project in projects)
            {
                listStaff.AddRange(positionInProjectBusiness.GetAll().Where(p => p.ProjectId == project).Select(a => a.StaffId));
            }
            var claims = new List<ClaimViewModel>();
            foreach (var item in listStaff)
            {
                claims.AddRange(claimBusiness.GetAll().Where(c => c.StaffId == item && c.Status == Status.Approved || c.Status == Status.Paid));
            }
            return View("ForMyVetting", claims);
        }

        [Authorize(Roles = "Finance")]
        public ActionResult FinanceApproved()
        {
            ViewBag.Title = "Approved";
            var claims = claimBusiness.GetAll().Where(c => c.Status == Status.Approved);
            return View(claims);
        }

        // GET: Claims/Details/5
        [Authorize(Roles = "Claimer,Admin")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ClaimViewModel claim = claimBusiness.Detail(id.Value);
            if (claim == null)
            {
                return HttpNotFound();
            }
            return View(claim);
        }
        [Authorize(Roles = "Claimer")]
        // GET: Claims/Create
        public ActionResult Create()
        {
            ViewBag.Title = "Create Claim";
            Session["listClaimDetail"] = listClaimDetail;

            string staffId = User.Identity.GetUserId();
            var staff = staffBusiness.Detail(staffId);
            ViewBag.StaffId = staffId;
            ViewBag.StaffName = staff.StaffName;
            ViewBag.StaffDepartment = staff.Department;
            var listProject = positionInProjectBusiness.GetAll().Where(p => p.StaffId == staffId).ToList();
            ViewBag.ProjectId = new SelectList(listProject, "ProjectId", "Project.ProjectName");

            return View();
        }
        [Authorize(Roles = "Claimer")]
        [HttpPost]
        public JsonResult CreateClaimDetail(int id, string date, string from, string to, string hours, string remarks)
        {
            listClaimDetail = (List<ClaimDetailViewModel>)Session["listClaimDetail"];


            ClaimDetailViewModel claimdetail = new ClaimDetailViewModel()
            {
                ClaimDetailId = id,
                Date = DateTime.Parse(date),
                FromTimeWork = DateTime.Parse(from),
                ToTimeWork = DateTime.Parse(to),
                TotalWorkHours = decimal.Parse(hours),
                Remarks = remarks,
            };
            listClaimDetail.Add(claimdetail);
            Session["listClaimDetail"] = listClaimDetail;
            return Json(true);
        }
        [Authorize(Roles = "Claimer")]
        [HttpPost]
        public JsonResult DeleteClaimDetail(int id)
        {
            List<ClaimDetailViewModel> listClaimDetail = (List<ClaimDetailViewModel>)Session["listClaimDetail"];

            if (listClaimDetail != null)
            {
                foreach (ClaimDetailViewModel item in listClaimDetail.ToList())
                {
                    if (item.ClaimDetailId == id)
                    {
                        listClaimDetail.Remove(item);                      
                    }
                }
            }
            Session["listClaimDetail"] = listClaimDetail;
            return Json(true);
        }
        // POST: Claims/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Claimer")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ClaimId,Status,TotalWorkingHour,Remarks,AuditTrail,StaffId")] ClaimViewModel claim, string save, string submitClaim)
        {
            if (ModelState.IsValid)
            {

                if (!string.IsNullOrEmpty(save))
                {
                    claim.Status = Status.Draft;
                    claim.AuditTrail = "Created on " + DateTime.Now + " by " + User.Identity.Name;
                }
                else if (!string.IsNullOrEmpty(submitClaim))
                {
                    claim.Status = Status.PenddingApproval;
                    claim.AuditTrail = "Created on " + DateTime.Now + " by " + User.Identity.Name
                        + "<br>Submited on " + DateTime.Now + " by " + User.Identity.Name;

                }
                listClaimDetail = (List<ClaimDetailViewModel>)Session["listClaimDetail"];
                foreach (var item in listClaimDetail)
                {
                    item.ClaimId = claim.ClaimId;
                    claim.ClaimDetails.Add(item);
                }
                claim.ModifiedDate = DateTime.Now;
                claim.StaffId = User.Identity.GetUserId();
                claimBusiness.Add(claim);
                var lastClaim = claimBusiness.GetLastestClaim();
                if (!string.IsNullOrEmpty(submitClaim))
                {
                    try
                    {
                        var projectName = projectBusiness.GetProjectName(lastClaim.ClaimId);
                        var pmName = projectBusiness.GetPMName(lastClaim.ClaimId);
                        var staffName = staffBusiness.GetStaffName(lastClaim.StaffId);
                        var staffId = lastClaim.StaffId;

                        string content = System.IO.File.ReadAllText(Server.MapPath("~/Content/EmailTemplate/EmailTemplate.html"));
                        content = content.Replace("{{PMName}}", pmName);
                        content = content.Replace("{{ProjectName}}", projectName);
                        content = content.Replace("{{StaffName}}", staffName);
                        content = content.Replace("{{StaffID}}", staffId);
                        string toMail = ConfigurationManager.AppSettings["ToEmailAddress"].ToString();
                        new MailHelper().SendMail(toMail, "Submit claim", content);
                    }
                    catch (Exception ex)
                    {

                    }
                }

                return RedirectToAction("Index", "Claims", new { status = "draft" });
            }

            ViewBag.StaffId = new SelectList(claimBusiness.GetAll(), "Id", "Email", claim.StaffId);
            return View(claim);
        }


        [Authorize(Roles = "Claimer")]
        // GET: Claims/Edit/5
        public ActionResult Edit(int? id)
        {
            ViewBag.Title = "Edit Claim";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ClaimViewModel claim = claimBusiness.Detail(id);
            if (claim == null)
            {
                return HttpNotFound();
            }

            string staffId = User.Identity.GetUserId();
            var staff = staffBusiness.Detail(staffId);
            ViewBag.StaffId = staffId;
            ViewBag.StaffName = staff.StaffName;
            ViewBag.StaffDepartment = staff.Department;
            var listProject = positionInProjectBusiness.GetAll().Where(p => p.StaffId == staffId).ToList();
            ViewBag.ProjectId = new SelectList(listProject, "ProjectId", "Project.ProjectName");
            Session["listClaimDetail"] = claim.ClaimDetails;
            return View(claim);
        }

        // POST: Claims/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Claimer")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ClaimId,Status,TotalWorkingHour,Remarks,AuditTrail,StaffId")] ClaimViewModel claim, string save, string submitClaim)
        {
            if (ModelState.IsValid)
            {
                if (!string.IsNullOrEmpty(save))
                {
                    claim.Status = Status.Draft;
                    claim.AuditTrail = "Updated on " + DateTime.Now + " by " + User.Identity.Name;
                }
                else if (!string.IsNullOrEmpty(submitClaim))
                {
                    claim.Status = Status.PenddingApproval;
                    claim.AuditTrail = "Updated on " + DateTime.Now + " by " + User.Identity.Name
                        + "<br>Submited on " + DateTime.Now + " by " + User.Identity.Name;

                }
                
                listClaimDetail = (List<ClaimDetailViewModel>)Session["listClaimDetail"];
                claim.ClaimDetails = listClaimDetail;
                //list nay la 1 list cos nhung claim ton tai roi va cai vua them
                



                claim.ModifiedDate = DateTime.Now;
                claim.StaffId = User.Identity.GetUserId();
                claimBusiness.Update(claim);
                return RedirectToAction("Index", "Claims", new { status = "draft" });
            }
            ViewBag.StaffId = new SelectList(staffBusiness.GetAll(), "Id", "FullName", claim.StaffId);
            return View(claim);
        }

        // GET: Claims/Delete/5
        [Authorize(Roles = "Claimer")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ClaimViewModel claim = claimBusiness.Detail(id.Value);
            if (claim == null)
            {
                return HttpNotFound();
            }
            return View(claim);
        }

        // POST: Claims/Delete/5
        [Authorize(Roles = "Claimer")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            claimBusiness.Delete(id);
            return RedirectToAction("Index");
        }


        // quyền này là Approver tức là thằng sếp PM hoặc clg đó
        [Authorize(Roles = "Approver")]
        public ActionResult ApproveOrRejectOrReturnClaim(int ClaimId, string actionClaim)
        {
            var claimToRequest = claimBusiness.Detail(ClaimId);
            if (actionClaim == "approve")
            {
                claimToRequest.Status = Status.Approved;
                claimToRequest.AuditTrail += "<br>Approved by " + User.Identity.Name + " on " + DateTime.Now;
            }
            else if (actionClaim == "reject")
            {
                claimToRequest.Status = Status.Rejected;
                claimToRequest.AuditTrail += "<br>Rejected by " + User.Identity.Name + " on " + DateTime.Now;
            }
            else if (actionClaim == "return")
            {
                claimToRequest.Status = Status.Draft;
                claimToRequest.AuditTrail += "<br>Returned by " + User.Identity.Name + " on " + DateTime.Now;
            }
            claimToRequest.ModifiedDate = DateTime.Now;
            claimBusiness.Update(claimToRequest);
            return RedirectToAction("ForMyVetting");
        }

        [Authorize(Roles = "Finance")]
        public ActionResult Finance(string status)
        {
            if (status == "approved")
            {
                ViewBag.Title = "Approved";
                var claims = claimBusiness.GetAll().Where(x => x.Status == Status.Approved);
                return View("FinanceApproved", claims);
            }
            else
            {
                ViewBag.Title = "Paid";
                var claims = claimBusiness.GetAll().Where(x => x.Status == Status.Paid);
                return View("FinancePaid", claims);
            }
        }


        public ActionResult CancelClaim(int ClaimId)
        {
            var claimToRequest = claimBusiness.Detail(ClaimId);
            if (claimToRequest.Status == Status.PenddingApproval)
            {
                claimToRequest.Status = Status.Draft;
                claimToRequest.ModifiedDate = DateTime.Now;
                claimToRequest.AuditTrail += "<br>Cancel by " + User.Identity.Name + " on " + DateTime.Now;
                claimBusiness.Update(claimToRequest);
            }
            return RedirectToAction("Index", new { status = "draft" });
        }

        [Authorize(Roles ="Finance")]
        [HttpPost]
        public ActionResult PaidClaim(IEnumerable<int> SelectedClaims)
        {
            foreach (var item in SelectedClaims)
            {
                var claimToRequest = claimBusiness.Detail(item);
                if (claimToRequest.Status == Status.Approved)
                {
                    claimToRequest.Status = Status.Paid;
                    claimToRequest.ModifiedDate = DateTime.Now;
                    claimToRequest.AuditTrail += "<br>Paid by " + User.Identity.Name + " on " + DateTime.Now;
                    claimBusiness.Update(claimToRequest);
                }
            }
            return RedirectToAction("Finance", new { status = "paid" });
        }

        // hàm download chiều viết 
        public void DownloadClaim()
        {
            var claims = claimBusiness.GetAll().Where(x => x.Status == Status.Paid && x.ModifiedDate.Value.Month == DateTime.Today.Month).ToList();
            DataTable dt = new DataTable();
            dt.Columns.Add("Id", typeof(int));
            dt.Columns.Add("Staff Name", typeof(string));
            dt.Columns.Add("Total Working Hour", typeof(decimal));
            dt.Columns.Add("Remarks", typeof(string));
            dt.Columns.Add("Paid Date", typeof(string));
            foreach (var item in claims)
            {
                DataRow dr = dt.NewRow();
                dr[0] = item.ClaimId;
                dr[1] = item.Staff.StaffName;
                dr[2] = item.TotalWorkingHour;
                dr[3] = item.Remarks;
                dr[4] = item.ModifiedDate.Value.ToString("dd/MM/yyyy");
                dt.Rows.Add(dr);
            }
            ExcelPackage excel = new ExcelPackage();
            var workSheet = excel.Workbook.Worksheets.Add("Sheet1");
            workSheet.Cells[1, 1].LoadFromDataTable(dt, true, TableStyles.None);
            using (var memoryStream = new MemoryStream())
            {
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment; filename=PaidClaims.xlsx");
                excel.SaveAs(memoryStream); memoryStream.WriteTo(Response.OutputStream);
                Response.Flush();
                Response.End();
            }
        }
    }
}
