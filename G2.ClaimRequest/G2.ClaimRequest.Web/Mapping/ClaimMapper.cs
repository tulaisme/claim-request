﻿using AutoMapper;
using G2.ClaimRequest.Core.Models;
using G2.ClaimRequest.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace G2.ClaimRequest.Web.Mapping
{
    public static class ClaimMapper
    {
        public static ClaimViewModel ToModel(this Claim claim)
        {
            var viewModel = Mapper.Map<ClaimViewModel>(claim);

            return viewModel;
        }

        public static Claim ToEntity(this ClaimViewModel claimViewModel)
        {
            var entity = Mapper.Map<Claim>(claimViewModel);
            return entity;
        }
    }
}