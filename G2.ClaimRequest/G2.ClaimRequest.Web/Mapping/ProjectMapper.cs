﻿using AutoMapper;
using G2.ClaimRequest.Core.Models;
using G2.ClaimRequest.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace G2.ClaimRequest.Web.Mapping
{
    public static class ProjectMapper
    {
        public static ProjectViewModel ToModel(this Project project)
        {
            var viewModel = Mapper.Map<ProjectViewModel>(project);
            return viewModel;
        }

        public static Project ToEntity(this ProjectViewModel projectViewModel)
        {
            var entity = Mapper.Map<Project>(projectViewModel);
            return entity;
        }
    }
}