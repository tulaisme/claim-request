﻿using AutoMapper;
using G2.ClaimRequest.Core.Models;
using G2.ClaimRequest.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace G2.ClaimRequest.Web.Mapping
{
    public static class ClaimDetailMapper
    {
        public static ClaimDetailViewModel ToModel(this ClaimDetail claim)
        {
            var viewModel = Mapper.Map<ClaimDetailViewModel>(claim);
            return viewModel;
        }

        public static ClaimDetail ToEntity(this ClaimDetailViewModel claimViewModel)
        {
            var entity = Mapper.Map<ClaimDetail>(claimViewModel);
            return entity;
        }
    }
}