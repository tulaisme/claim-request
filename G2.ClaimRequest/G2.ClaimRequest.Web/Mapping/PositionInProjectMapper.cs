﻿using AutoMapper;
using G2.ClaimRequest.Core.Models;
using G2.ClaimRequest.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace G2.ClaimRequest.Web.Mapping
{
    public static class PositionInProjectMapper
    {
        public static PositionInProjectViewModel ToModel(this PositionInProject positionInProject)
        {
            var viewModel = Mapper.Map<PositionInProjectViewModel>(positionInProject);
            return viewModel;
        }

        public static PositionInProject ToEntity(this PositionInProjectViewModel positionInProjectViewModel)
        {
            var entity = Mapper.Map<PositionInProject>(positionInProjectViewModel);
            return entity;
        }
    }
}