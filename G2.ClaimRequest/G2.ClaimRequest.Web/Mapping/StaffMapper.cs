﻿using AutoMapper;
using G2.ClaimRequest.Core.Models;
using G2.ClaimRequest.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace G2.ClaimRequest.Web.Mapping
{
    public static class StaffMapper
    {
        public static StaffViewModel ToModel(this Staff staff)
        {
            var viewModel = Mapper.Map<StaffViewModel>(staff);
            return viewModel;
        }

        public static Staff ToEntity(this StaffViewModel staffViewModel)
        {
            var entity = Mapper.Map<Staff>(staffViewModel);
            return entity;
        }
    }
}