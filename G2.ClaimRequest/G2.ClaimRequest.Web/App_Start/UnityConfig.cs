using G2.ClaimRequest.Core.Data;
using G2.ClaimRequest.Services.IRepositories;
using G2.ClaimRequest.Services.Repositories;
using G2.ClaimRequest.Web.Business;
using G2.ClaimRequest.Web.Controllers;
using IdentitySample.Controllers;
using System.Web.Mvc;
using Unity;
using Unity.Injection;
using Unity.Mvc5;

namespace G2.ClaimRequest.Web
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. container.RegisterType<ITestService, TestService>();
            var ctr = new InjectionConstructor(typeof(ClaimDbContext));
            // Dat no bao them cai nay nhung cung khong biet de lam gi -- khong phai code cua toi
            //container.RegisterType<AccountController>(ctr);

            container.RegisterType<ManageController>(new InjectionConstructor());
            container.RegisterType<UsersAdminController>(new InjectionConstructor());
            container.RegisterType<RolesAdminController>(new InjectionConstructor());


            container.RegisterType<ClaimsController>(ctr);
            //container.RegisterType<ClaimDetailsController>(new InjectionConstructor());
            //container.RegisterType<ProjectsController>(new InjectionConstructor());
            //container.RegisterType<StaffsController>(new InjectionConstructor());

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));

        }
    }
}