﻿using AutoMapper;
using G2.ClaimRequest.Core.Models;
using G2.ClaimRequest.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace G2.ClaimRequest.Web.App_Start
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<ClaimDetail, ClaimDetailViewModel>().MaxDepth(3);
            CreateMap<ClaimDetailViewModel, ClaimDetail>().MaxDepth(3);
            CreateMap<Claim, ClaimViewModel>().MaxDepth(3);
            CreateMap<ClaimViewModel, Claim>().MaxDepth(3);
            CreateMap<Project, ProjectViewModel>().MaxDepth(3);
            CreateMap<Staff, StaffViewModel>().MaxDepth(3);
            CreateMap<StaffViewModel, Staff>().MaxDepth(3);
            CreateMap<ProjectViewModel, Project>().MaxDepth(3);
            CreateMap<PositionInProject, PositionInProjectViewModel>().MaxDepth(3);
            CreateMap<PositionInProjectViewModel, PositionInProject>().MaxDepth(3);
        }
    }
}