﻿using G2.ClaimRequest.Core.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;

namespace G2.ClaimRequest.Core.Data
{
    public class ClaimDbContext : IdentityDbContext
    {
        public ClaimDbContext() : base("ClaimConnection")
        {
            Database.SetInitializer(new CreateDatabaseIfNotExists<ClaimDbContext>());
        }
        public DbSet<Claim> Claims { get; set; }
        public DbSet<Staff> Staffs { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<ClaimDetail> ClaimDetails { get; set; }

        public DbSet<PositionInProject> PositionInProjects { get; set; }

        //protected override void OnModelCreating(DbModelBuilder modelBuilder)
        //{
        //    modelBuilder.Entity<IdentityUserRole>().HasKey(i => new { i.UserId, i.RoleId });
        //    modelBuilder.Entity<IdentityUserLogin>().HasKey(i => i.UserId);

        //    //modelBuilder.Entity<PositionInProject>()
        //    //    .HasKey(p => new { p.ProjectId, p.StaffId });
        //}

        public static ClaimDbContext Create()
        {
            return new ClaimDbContext();
        }
    }
}
