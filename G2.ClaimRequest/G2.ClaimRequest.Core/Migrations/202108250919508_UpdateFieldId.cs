namespace G2.ClaimRequest.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateFieldId : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ClaimDetails", "Claim_ClaimId", "dbo.Claims");
            DropIndex("dbo.ClaimDetails", new[] { "Claim_ClaimId" });
            RenameColumn(table: "dbo.ClaimDetails", name: "Claim_ClaimId", newName: "ClaimId");
            RenameColumn(table: "dbo.Claims", name: "Staff_Id", newName: "StaffId");
            RenameIndex(table: "dbo.Claims", name: "IX_Staff_Id", newName: "IX_StaffId");
            AlterColumn("dbo.ClaimDetails", "ClaimId", c => c.Int(nullable: false));
            CreateIndex("dbo.ClaimDetails", "ClaimId");
            AddForeignKey("dbo.ClaimDetails", "ClaimId", "dbo.Claims", "ClaimId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ClaimDetails", "ClaimId", "dbo.Claims");
            DropIndex("dbo.ClaimDetails", new[] { "ClaimId" });
            AlterColumn("dbo.ClaimDetails", "ClaimId", c => c.Int());
            RenameIndex(table: "dbo.Claims", name: "IX_StaffId", newName: "IX_Staff_Id");
            RenameColumn(table: "dbo.Claims", name: "StaffId", newName: "Staff_Id");
            RenameColumn(table: "dbo.ClaimDetails", name: "ClaimId", newName: "Claim_ClaimId");
            CreateIndex("dbo.ClaimDetails", "Claim_ClaimId");
            AddForeignKey("dbo.ClaimDetails", "Claim_ClaimId", "dbo.Claims", "ClaimId");
        }
    }
}
