namespace G2.ClaimRequest.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitDb : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ClaimDetails",
                c => new
                    {
                        ClaimDetailId = c.Int(nullable: false, identity: true),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        FromTimeWork = c.DateTime(nullable: false),
                        ToTimeWork = c.DateTime(nullable: false),
                        Claim_ClaimId = c.Int(),
                    })
                .PrimaryKey(t => t.ClaimDetailId)
                .ForeignKey("dbo.Claims", t => t.Claim_ClaimId)
                .Index(t => t.Claim_ClaimId);
            
            CreateTable(
                "dbo.Claims",
                c => new
                    {
                        ClaimId = c.Int(nullable: false, identity: true),
                        Status = c.Int(nullable: false),
                        TotalWorkingHour = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Remarks = c.String(maxLength: 500),
                        AuditTrail = c.String(maxLength: 1000),
                        Staff_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.ClaimId)
                .ForeignKey("dbo.AspNetUsers", t => t.Staff_Id)
                .Index(t => t.Staff_Id);
            
            CreateTable(
                "dbo.PositionInProject",
                c => new
                    {
                        StaffId = c.String(nullable: false, maxLength: 128),
                        ProjectId = c.Int(nullable: false),
                        Position = c.String(maxLength: 30),
                    })
                .PrimaryKey(t => new { t.StaffId, t.ProjectId })
                .ForeignKey("dbo.AspNetUsers", t => t.StaffId, cascadeDelete: true)
                .ForeignKey("dbo.Projects", t => t.ProjectId, cascadeDelete: true)
                .Index(t => t.StaffId)
                .Index(t => t.ProjectId);
            
            CreateTable(
                "dbo.Projects",
                c => new
                    {
                        ProjectId = c.Int(nullable: false, identity: true),
                        ProjectName = c.String(maxLength: 200),
                        ProjectCode = c.String(maxLength: 20, unicode: false),
                        From = c.DateTime(nullable: false),
                        To = c.DateTime(nullable: false),
                        PMId = c.String(maxLength: 128),
                        QAId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.ProjectId)
                .ForeignKey("dbo.AspNetUsers", t => t.PMId)
                .ForeignKey("dbo.AspNetUsers", t => t.QAId)
                .Index(t => t.PMId)
                .Index(t => t.QAId);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                        StaffName = c.String(maxLength: 200),
                        Department = c.String(maxLength: 200),
                        JobRank = c.String(maxLength: 200),
                        Salary = c.Decimal(precision: 18, scale: 2),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.PositionInProject", "ProjectId", "dbo.Projects");
            DropForeignKey("dbo.Projects", "QAId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Projects", "PMId", "dbo.AspNetUsers");
            DropForeignKey("dbo.PositionInProject", "StaffId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Claims", "Staff_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.ClaimDetails", "Claim_ClaimId", "dbo.Claims");
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.Projects", new[] { "QAId" });
            DropIndex("dbo.Projects", new[] { "PMId" });
            DropIndex("dbo.PositionInProject", new[] { "ProjectId" });
            DropIndex("dbo.PositionInProject", new[] { "StaffId" });
            DropIndex("dbo.Claims", new[] { "Staff_Id" });
            DropIndex("dbo.ClaimDetails", new[] { "Claim_ClaimId" });
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.Projects");
            DropTable("dbo.PositionInProject");
            DropTable("dbo.Claims");
            DropTable("dbo.ClaimDetails");
        }
    }
}
