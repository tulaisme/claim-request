namespace G2.ClaimRequest.Core.Migrations
{
    using G2.ClaimRequest.Core.Data;
    using G2.ClaimRequest.Core.Models;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<G2.ClaimRequest.Core.Data.ClaimDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(G2.ClaimRequest.Core.Data.ClaimDbContext context)
        {
            var manager = new UserManager<Staff>(new UserStore<Staff>(new ClaimDbContext()));
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new ClaimDbContext()));




            var user = new Staff()
            {
                UserName = "admin@gmail.com",
                Email = "admin@gmail.com",
                StaffName = "Luong DInh Nam"
            };

            manager.Create(user, "Admin@123");

            if (!roleManager.Roles.Any())
            {
                roleManager.Create(new IdentityRole { Name = "Admin" });
                roleManager.Create(new IdentityRole { Name = "Claimer" });
                roleManager.Create(new IdentityRole { Name = "Approver" });
                roleManager.Create(new IdentityRole { Name = "Finance" });
            }

            var blogOwnerUser = manager.FindByEmail("admin@gmail.com");
            manager.AddToRoles(blogOwnerUser.Id, new string[] { "Admin" });

            var claimDetails = new ClaimDetail[]
            {
                new ClaimDetail()
                {
                    Date = new DateTime(2021,08,02),
                    FromTimeWork = DateTime.Now,
                    ToTimeWork = DateTime.Now,
                },
                new ClaimDetail()
                {
                    Date = new DateTime(2021,02,02),
                    FromTimeWork = DateTime.Now,
                    ToTimeWork = DateTime.Now,
                },
            };
            context.ClaimDetails.AddRange(claimDetails);

            var claims = new Claim[]
            {
                new Claim()
                {
                    Remarks = "fdsgfs",
                    Status = Status.Draft,
                    TotalWorkingHour = 8,
                    AuditTrail = "safsdsdf",
                },
                new Claim()
                {
                    Remarks = "sdxfcghjkl",
                    Status = Status.Approved,
                    TotalWorkingHour = 5,
                    AuditTrail = "gxhcjvgkbhj",
                },
            };
            claims[0].ClaimDetails = new List<ClaimDetail>() { claimDetails[0], claimDetails[1] };
            claims[1].ClaimDetails = new List<ClaimDetail>() { claimDetails[0]};
            context.Claims.AddRange(claims);

            var projects = new Project[]
            {
                new Project()
                {
                    From = DateTime.Now,
                    ProjectCode = "dasd",
                    ProjectName = "Du an 1",
                    To = DateTime.Now,
                },
                new Project()
                {
                    From = DateTime.Now,
                    ProjectCode = "dafssdfsddf",
                    ProjectName = "Du an 2",
                    To = DateTime.Now,
                },
            };
            context.Projects.AddRange(projects);

            context.SaveChanges();
        }
    }
}
