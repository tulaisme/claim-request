namespace G2.ClaimRequest.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initializer : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ClaimDetails",
                c => new
                    {
                        ClaimDetailId = c.Int(nullable: false, identity: true),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        FromTimeWork = c.DateTime(nullable: false),
                        ToTimeWork = c.DateTime(nullable: false),
                        Claim_ClaimId = c.Int(),
                    })
                .PrimaryKey(t => t.ClaimDetailId)
                .ForeignKey("dbo.Claims", t => t.Claim_ClaimId)
                .Index(t => t.Claim_ClaimId);
            
            CreateTable(
                "dbo.Claims",
                c => new
                    {
                        ClaimId = c.Int(nullable: false, identity: true),
                        Status = c.Int(nullable: false),
                        TotalWorkingHour = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Remarks = c.String(maxLength: 500),
                        AuditTrail = c.String(maxLength: 1000),
                    })
                .PrimaryKey(t => t.ClaimId);
            
            CreateTable(
                "dbo.Projects",
                c => new
                    {
                        ProjectId = c.Int(nullable: false, identity: true),
                        ProjectName = c.String(maxLength: 200),
                        ProjectCode = c.String(maxLength: 20, unicode: false),
                        From = c.DateTime(nullable: false),
                        To = c.DateTime(nullable: false),
                        Staff_Id = c.String(maxLength: 128),
                        PM_Id = c.String(maxLength: 128),
                        QA_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.ProjectId)
                .ForeignKey("dbo.AspNetUsers", t => t.Staff_Id)
                .ForeignKey("dbo.AspNetUsers", t => t.PM_Id)
                .ForeignKey("dbo.AspNetUsers", t => t.QA_Id)
                .Index(t => t.Staff_Id)
                .Index(t => t.PM_Id)
                .Index(t => t.QA_Id);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                        StaffName = c.String(maxLength: 200),
                        Department = c.String(maxLength: 200),
                        JobRank = c.String(maxLength: 200),
                        Salary = c.Decimal(precision: 18, scale: 2),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                        Project_ProjectId = c.Int(),
                        Project_ProjectId1 = c.Int(),
                        Project_ProjectId2 = c.Int(),
                        Project_ProjectId3 = c.Int(),
                        Project_ProjectId4 = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Projects", t => t.Project_ProjectId)
                .ForeignKey("dbo.Projects", t => t.Project_ProjectId1)
                .ForeignKey("dbo.Projects", t => t.Project_ProjectId2)
                .ForeignKey("dbo.Projects", t => t.Project_ProjectId3)
                .ForeignKey("dbo.Projects", t => t.Project_ProjectId4)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex")
                .Index(t => t.Project_ProjectId)
                .Index(t => t.Project_ProjectId1)
                .Index(t => t.Project_ProjectId2)
                .Index(t => t.Project_ProjectId3)
                .Index(t => t.Project_ProjectId4);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.AspNetUsers", "Project_ProjectId4", "dbo.Projects");
            DropForeignKey("dbo.AspNetUsers", "Project_ProjectId3", "dbo.Projects");
            DropForeignKey("dbo.AspNetUsers", "Project_ProjectId2", "dbo.Projects");
            DropForeignKey("dbo.Projects", "QA_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.Projects", "PM_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUsers", "Project_ProjectId1", "dbo.Projects");
            DropForeignKey("dbo.AspNetUsers", "Project_ProjectId", "dbo.Projects");
            DropForeignKey("dbo.Projects", "Staff_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.ClaimDetails", "Claim_ClaimId", "dbo.Claims");
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", new[] { "Project_ProjectId4" });
            DropIndex("dbo.AspNetUsers", new[] { "Project_ProjectId3" });
            DropIndex("dbo.AspNetUsers", new[] { "Project_ProjectId2" });
            DropIndex("dbo.AspNetUsers", new[] { "Project_ProjectId1" });
            DropIndex("dbo.AspNetUsers", new[] { "Project_ProjectId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.Projects", new[] { "QA_Id" });
            DropIndex("dbo.Projects", new[] { "PM_Id" });
            DropIndex("dbo.Projects", new[] { "Staff_Id" });
            DropIndex("dbo.ClaimDetails", new[] { "Claim_ClaimId" });
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.Projects");
            DropTable("dbo.Claims");
            DropTable("dbo.ClaimDetails");
        }
    }
}
