namespace G2.ClaimRequest.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class loaga : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "FullName", c => c.String(maxLength: 200));
            AlterColumn("dbo.AspNetUsers", "JobRank", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.AspNetUsers", "JobRank", c => c.String(maxLength: 200));
            DropColumn("dbo.AspNetUsers", "FullName");
        }
    }
}
