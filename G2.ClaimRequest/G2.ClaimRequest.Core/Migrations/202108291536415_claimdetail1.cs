namespace G2.ClaimRequest.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class claimdetail1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ClaimDetails", "Remarks", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ClaimDetails", "Remarks");
        }
    }
}
