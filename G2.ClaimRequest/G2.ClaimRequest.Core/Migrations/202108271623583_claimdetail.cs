namespace G2.ClaimRequest.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class claimdetail : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ClaimDetails", "Date", c => c.DateTime(nullable: false));
            AddColumn("dbo.ClaimDetails", "TotalWorkHours", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            DropColumn("dbo.ClaimDetails", "StartDate");
            DropColumn("dbo.ClaimDetails", "EndDate");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ClaimDetails", "EndDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.ClaimDetails", "StartDate", c => c.DateTime(nullable: false));
            DropColumn("dbo.ClaimDetails", "TotalWorkHours");
            DropColumn("dbo.ClaimDetails", "Date");
        }
    }
}
