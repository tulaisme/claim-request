namespace G2.ClaimRequest.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class date : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Claims", "ModifiedDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Claims", "ModifiedDate", c => c.DateTime(nullable: false));
        }
    }
}
