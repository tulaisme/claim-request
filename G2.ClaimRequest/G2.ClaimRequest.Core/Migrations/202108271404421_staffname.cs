namespace G2.ClaimRequest.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class staffname : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.AspNetUsers", "FullName");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AspNetUsers", "FullName", c => c.String(maxLength: 200));
        }
    }
}
