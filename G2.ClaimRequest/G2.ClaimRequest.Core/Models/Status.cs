﻿using System.ComponentModel.DataAnnotations;

namespace G2.ClaimRequest.Core.Models
{
    public enum Status
    {
        Draft,
        [Display(Name ="Pendding Approval")]
        PenddingApproval,
        Approved,
        Paid,
        Rejected,
        Cancelled
    }
}
