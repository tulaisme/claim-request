﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace G2.ClaimRequest.Core.Models
{
    [Table("PositionInProject")]
    public class PositionInProject
    {
        [Key, Column(Order = 0)]
        public string StaffId { get; set; }
        [ForeignKey("StaffId")]
        public Staff Staff { get; set; }

        [Key, Column(Order = 1)]
        public int ProjectId { get; set; }
        [ForeignKey("ProjectId")]
        public virtual Project Project { get; set; }

        [MaxLength(30)]
        public string Position { get; set; }
    }
}
