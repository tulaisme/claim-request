﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace G2.ClaimRequest.Core.Models
{
    public class Staff : IdentityUser
    {
        [MaxLength(200)]
        public string StaffName { get; set; }
        [MaxLength(200)]
        public string Department { get; set; }
        public string JobRank { get; set; }
        public decimal Salary { get; set; }
        public virtual ICollection<Claim> ClaimRequests { get; set; }
        public virtual ICollection<PositionInProject> Projects { get; set; }
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<Staff> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }
}
