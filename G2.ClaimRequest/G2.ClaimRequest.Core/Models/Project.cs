﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace G2.ClaimRequest.Core.Models
{
    public class Project
    {
        public int ProjectId { get; set; }
        [MaxLength(200)]
        public string ProjectName { get; set; }
        [Column(TypeName = "varchar")]
        [MaxLength(20)]
        public string ProjectCode { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }

        public virtual ICollection<PositionInProject> Staffs { get; set; }

        public string PMId { get; set; }
        public virtual Staff PM { get; set; }

        public string QAId { get; set; }
        public virtual Staff QA { get; set; }
        //public virtual ICollection<Staff> TechnicalLead { get; set; }
        //public virtual ICollection<Staff> BA { get; set; }
        //public virtual ICollection<Staff> Developers { get; set; }
        //public virtual ICollection<Staff> Testers { get; set; }
        //public virtual ICollection<Staff> TechnicalConsultancy { get; set; }

    }
}
