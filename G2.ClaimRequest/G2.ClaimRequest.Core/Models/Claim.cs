﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace G2.ClaimRequest.Core.Models
{
    public class Claim
    {
        public int ClaimId { get; set; }
        public Status Status { get; set; }
        public decimal TotalWorkingHour { get; set; }

        [MaxLength(500)]
        public string Remarks { get; set; }

        [MaxLength(1000)]
        public string AuditTrail { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public virtual ICollection<ClaimDetail> ClaimDetails { get; set; }
        public string StaffId { get; set; }
        public virtual Staff Staff { get; set; }
    }
}
