﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace G2.ClaimRequest.Core.Models
{
    public class ClaimDetail
    {
        [Key]
        public int ClaimDetailId { get; set; }
        public DateTime Date { get; set; }
        public DateTime FromTimeWork { get; set; }
        public DateTime ToTimeWork { get; set; }
        public decimal TotalWorkHours { get; set; }
        public string Remarks { get; set; }
        public int ClaimId { get; set; }
        public virtual Claim Claim { get; set; }
    }
}
